package semana5.estado;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Registro
 */
@WebServlet(name = "registro", urlPatterns = { "/registro" })
public class Registro extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registro() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Obtener las datos de usuario
		String nombre = request.getParameter("nombre");
		String apellidos = request.getParameter("apellidos");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Persona p = new Persona(nombre, apellidos, email, password);
		
		// Guardar el usuario
		ArrayList<Persona> usuarios;
		ServletContext context = getServletContext();
		synchronized (context) {
			usuarios = (ArrayList<Persona>) context.getAttribute("usuarios");
			if (usuarios == null)
				usuarios = new ArrayList<Persona>();
			usuarios.add(p);
			context.setAttribute("usuarios", usuarios);
		}
		
		// Mostrar la lista de usuarios
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.append("<!DOCTYPE html>");
		out.append("<html>");
		out.append("<head>");
		out.append("</head>");
		out.append("<body>");
		out.append("<table>");
		out.append("<tr>");
		out.append("<th>Nombre</th>");
		out.append("<th>Apellidos</th>");
		out.append("<th>Email</th>");
		out.append("<th>Password</th>");
		out.append("</tr>");
		for(Persona u : usuarios) {
			out.append("<tr>");
			out.append("<td>"+ u.getNombre() +"</td>");
			out.append("<th>"+ u.getApellido() +"</td>");
			out.append("<td>"+ u.getEmail() +"</td>");
			out.append("<td>"+ u.getPassword() +"</td>");
			out.append("</tr>");
		}
		out.append("</table>");
		out.append("</body>");
		out.append("</html>");
	}

}
