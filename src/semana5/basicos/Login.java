package semana5.basicos;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet(name = "login", urlPatterns = { "/login" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private HashMap<String, String> usuarios;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        this.usuarios = new HashMap<String, String>();
        this.usuarios.put("admin", "1234");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String usuario = request.getParameter("usuario");
		String password = request.getParameter("password");
		
		System.out.println(usuario);
		System.out.println(password);
		
		Date hoy = new Date();
		request.setAttribute("fecha", hoy);
		
		HttpSession session = request.getSession();
		
		RequestDispatcher rd = null;
		if (this.usuarios.containsKey(usuario)) {
			String pwdAlmacenada = this.usuarios.get(usuario);
			if (password != null && password.equals(pwdAlmacenada)) {
				rd = request.getRequestDispatcher("/bienvenido");
//				response.sendRedirect("/ProyectosWeb/bienvenido?usuario="+ usuario);
			}
		} else {
			// Usuario no existe
			rd = request.getRequestDispatcher("/error.html");
//			response.sendRedirect("/ProyectosWeb/error.html");
		}
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
